msgid ""
msgstr ""
"Project-Id-Version: cs-cart-latest\n"
"Language-Team: Lithuanian\n"
"Language: lt_LT\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: cs-cart-latest\n"
"X-Crowdin-Language: lt\n"
"X-Crowdin-File: /addons/discussion.po\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Last-Translator: cscart <zeke@cs-cart.com>\n"
"PO-Revision-Date: 2016-03-17 08:58-0400\n"

msgctxt "Addons::name::discussion"
msgid "Comments and reviews"
msgstr "Komentarai ir atsiliepimai"

msgctxt "Addons::description::discussion"
msgid "Lets customers leave comments to products, categories, orders, etc., review products and write testimonials"
msgstr "Leidžia klientams komentuoti prekes, kategorijas, užsakymus ir rašyti atsiliepimus"

msgctxt "Languages::use_for_discussion"
msgid "Comments and reviews forms"
msgstr "Komentarų ir atsiliepimų forma"

msgctxt "Languages::comments_and_reviews_menu_description"
msgid "Customer comments and reviews on products, categories, orders, etc."
msgstr "Klientų pastabos ir atsiliepimai apie prekes, kategorijas, užsakymus ir t. t."

msgctxt "Languages::discussion_title_category"
msgid "Reviews"
msgstr "Atsiliepimai"

msgctxt "Languages::discussion_title_giftreg"
msgid "Guestbook"
msgstr "Svečių knyga"

msgctxt "Languages::discussion_title_home_page_menu_description"
msgid "Manage the testimonials from your store visitors."
msgstr "Valdyti jūsų parduotuvės lankytojų atsiliepimus."

msgctxt "Languages::discussion_title_order"
msgid "Communication"
msgstr "Komunikacijos"

msgctxt "Languages::discussion_title_page"
msgid "Comments"
msgstr "Komentarai"

msgctxt "Languages::discussion_title_product"
msgid "Reviews"
msgstr "Atsiliepimai"

msgctxt "Languages::discussion_title_return"
msgid "Communication"
msgstr "Komunikacijos"

msgctxt "Languages::no_posts_found"
msgid "No posts found"
msgstr "Įrašų nėra"

msgctxt "Languages::your_rating"
msgid "Your rating"
msgstr "Jūsų reitingas"

msgctxt "Languages::comments_and_reviews"
msgid "Comments and reviews"
msgstr "Komentarai ir atsiliepimai"

msgctxt "Languages::discussion_title_home_page"
msgid "Testimonials"
msgstr "Atsiliepimai"

msgctxt "Languages::text_enabled_testimonials_notice"
msgid "Testimonials are disabled. To enable, please go to <a href=\"[link]\">Comments and reviews: options</a> and select the \"Communication\", \"Rating\" or \"Communication and Rating\" value in \"Testimonials\" field."
msgstr "Atsiliepimai yra išjungti. Norėdami įgalinti, eikite į <a href=\"[link]\"> Komentarai ir atsiliepimai: funkcijos</a> ir pasirinkite \"Bendravimas\",  \"Įvertinimas\" arba \"Atsiliepimai\" lauką."

msgctxt "Languages::discussion_title_company"
msgid "Reviews"
msgstr "Atsiliepimai"

msgctxt "Languages::communication"
msgid "Communication"
msgstr "Komunikacijos"

msgctxt "Languages::rating"
msgid "Rating"
msgstr "Reitingas"

msgctxt "Languages::cant_find_thread"
msgid "Thread can not be found."
msgstr "Pokalbis nerastas."

msgctxt "Languages::error_already_posted"
msgid "You have posted in this discussion already"
msgstr "Jūs jau dalyvavote šioje diskusijoje"

msgctxt "Languages::text_thank_you_for_post"
msgid "Thank you for your post"
msgstr "Ačiū už jūsų pranešimą"

msgctxt "Languages::text_post_pended"
msgid "Your post will be checked before it gets published."
msgstr "Jūsų pranešimas bus tikrinamas prieš jį paskelbiant."

msgctxt "Languages::discussion"
msgid "Comments and reviews"
msgstr "Komentarai ir atsiliepimai"

msgctxt "Languages::text_new_post_notification"
msgid "This is a notification of a new post to"
msgstr "Tai yra pranešimas apie naują žinutę"

msgctxt "Languages::text_approval_notice"
msgid "This post needs approving"
msgstr "Šį pranešimą reikia patvirtinti"

msgctxt "Languages::latest_reviews"
msgid "Latest comments & reviews"
msgstr "Naujausi komentarai ir atsiliepimai"

msgctxt "Languages::comment_by"
msgid "Comment by"
msgstr "Komentaras"

msgctxt "Languages::and"
msgid "and"
msgstr "ir"

msgctxt "Languages::add_post"
msgid "Add post"
msgstr "Pridėti pranešimą"

msgctxt "Languages::not_approved"
msgid "Not approved"
msgstr "Nepatvirtinta"

msgctxt "Languages::discussion_manager"
msgid "Comments and reviews"
msgstr "Komentarai ir atsiliepimai"

msgctxt "Languages::testimonials"
msgid "Testimonials"
msgstr "Atsiliepimai"

msgctxt "Languages::privileges.view_discussions"
msgid "View comments and reviews"
msgstr "Rodyti komentarus ir atsiliepimus"

msgctxt "Languages::privileges.manage_discussions"
msgid "Manage comments and reviews"
msgstr "Tvarkyti Komentarus ir atsiliepimus"

msgctxt "Languages::block_testimonials"
msgid "Testimonials"
msgstr "Atsiliepimai"

msgctxt "Languages::block_testimonials_description"
msgid "User testimonials"
msgstr "Vartotojo atsiliepimai"

msgctxt "Languages::sort_by_rating_asc"
msgid "Sort by rating: Low to High"
msgstr "Rūšiuoti pagal reitingą: nuo mažo iki aukšto"

msgctxt "Languages::sort_by_rating_desc"
msgid "Sort by rating"
msgstr "Rūšiuoti pagal reitingą"

msgctxt "Languages::reviews"
msgid "review|reviews"
msgstr "Atsiliepimas| Atsiliepimai"

msgctxt "Languages::write_review"
msgid "Write a review"
msgstr "Parašyti atsiliepimą"

msgctxt "SettingsSections::discussion::products"
msgid "Products"
msgstr "Prekės"

msgctxt "SettingsSections::discussion::categories"
msgid "Categories"
msgstr "Kategorijos"

msgctxt "SettingsSections::discussion::orders"
msgid "Orders"
msgstr "Užsakymai"

msgctxt "SettingsSections::discussion::gift_registry"
msgid "Gift registry"
msgstr "Dovanų registras"

msgctxt "SettingsSections::discussion::pages"
msgid "Pages"
msgstr "Puslapiai"

msgctxt "SettingsSections::discussion::testimonials"
msgid "Testimonials"
msgstr "Atsiliepimai"

msgctxt "SettingsSections::discussion::companies"
msgid "Vendors"
msgstr "Parduotuvės"

msgctxt "SettingsOptions::discussion::product_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::product_post_approval"
msgid "Administrator must approve posts submitted by"
msgstr "Administratorius turi patvirtinti pateiktus pranešimus"

msgctxt "SettingsOptions::discussion::product_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiamas tik vienas postas iš vieno IP"

msgctxt "SettingsOptions::discussion::product_notification_email"
msgid "Send notifications to this E-mail"
msgstr "Siųsti pranešimus į šį el. adresą"

msgctxt "SettingsOptions::discussion::product_notify_vendor"
msgid "Send notifications to vendor"
msgstr "Siųsti pranešimus parduotuvės savininkui"

msgctxt "SettingsOptions::discussion::product_share_discussion"
msgid "Share discussion with product"
msgstr "Bendrinti diskusiją su preke"

msgctxt "SettingsOptions::discussion::category_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::category_post_approval"
msgid "Administrator must approve posts submitted by"
msgstr "Administratorius turi patvirtinti pateiktus pranešimus"

msgctxt "SettingsOptions::discussion::category_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiama tik vieną postą nuo vieno IP"

msgctxt "SettingsOptions::discussion::category_notification_email"
msgid "Send notifications to this E-mail"
msgstr "Siųsti pranešimus į šį el. adresą"

msgctxt "SettingsOptions::discussion::order_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::order_initiate"
msgid "Allow customer to initiate discussion"
msgstr "Leisti pirkėjams inicijuoti diskusijas"

msgctxt "SettingsOptions::discussion::order_notify_vendor"
msgid "Send notifications to vendor"
msgstr "Siųsti pranešimus parduotuvės savininkui"

msgctxt "SettingsOptions::discussion::giftreg_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::giftreg_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiama tik vieną postą nuo vieno IP"

msgctxt "SettingsOptions::discussion::page_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::page_post_approval"
msgid "Administrator must approve posts submitted by"
msgstr "Administratorius turi patvirtinti pateiktus pranešimus"

msgctxt "SettingsOptions::discussion::page_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiama tik vieną postą nuo vieno IP"

msgctxt "SettingsOptions::discussion::page_notification_email"
msgid "Send notifications to this E-mail"
msgstr "Siųsti pranešimus į šį el. adresą"

msgctxt "SettingsOptions::discussion::page_notify_vendor"
msgid "Send notifications to vendor"
msgstr "Siųsti pranešimus parduotuvės savininkui"

msgctxt "SettingsOptions::discussion::page_share_discussion"
msgid "Share discussion with page"
msgstr "Bendrinti diskusijų puslapį"

msgctxt "SettingsOptions::discussion::home_page_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::home_page_post_approval"
msgid "Administrator must approve posts submitted by"
msgstr "Administratorius turi patvirtinti pateiktus pranešimus"

msgctxt "SettingsOptions::discussion::home_page_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiama tik vieną postą nuo vieno IP"

msgctxt "SettingsOptions::discussion::home_page_notification_email"
msgid "Send notifications to this E-mail"
msgstr "Siųsti pranešimus į šį el. adresą"

msgctxt "SettingsOptions::discussion::home_page_testimonials"
msgid "Testimonials"
msgstr "Atsiliepimai"

msgctxt "SettingsOptions::discussion::testimonials_from_all_stores"
msgid "Show testimonials from all stores"
msgstr "Rodyti atsiliepimų iš visų parduotuvių"

msgctxt "SettingsOptions::discussion::company_discussion_type"
msgid "Reviews"
msgstr "Atsiliepimai"

msgctxt "SettingsOptions::discussion::company_only_buyers"
msgid "Allow to post only for the customers who has bought somethins from the vendor"
msgstr "Leisti rašyti tik tiems pirkėjams, kurie tik pirko ką nors"

msgctxt "SettingsOptions::discussion::company_posts_per_page"
msgid "Posts per page"
msgstr "Pranešimų skaičius puslapyje"

msgctxt "SettingsOptions::discussion::company_post_approval"
msgid "Administrator must approve posts submitted by"
msgstr "Administratorius turi patvirtinti pateiktus pranešimus"

msgctxt "SettingsOptions::discussion::company_post_ip_check"
msgid "Only one post from one IP is allowed"
msgstr "Leidžiama tik vieną postą nuo vieno IP"

msgctxt "SettingsOptions::discussion::company_notification_email"
msgid "Send notifications to this E-mail"
msgstr "Siųsti pranešimus į šį el. adresą"

msgctxt "SettingsOptions::discussion::company_notify_vendor"
msgid "Send notifications to vendor"
msgstr "Siųsti pranešimus parduotuvės savininkui"

msgctxt "SettingsVariants::discussion::company_post_approval::any"
msgid "Any customer"
msgstr "Visiems pirkėjams"

msgctxt "SettingsVariants::discussion::company_post_approval::anonymous"
msgid "Only anonymous customers"
msgstr "Tik anoniminiai pirkėjai"

msgctxt "SettingsVariants::discussion::company_post_approval::disabled"
msgid "No approval needed"
msgstr "Nereikia patvirtinimo"

msgctxt "SettingsVariants::discussion::company_discussion_type::B"
msgid "Communication and Rating"
msgstr "Komunikacijos ir vertinimas"

msgctxt "SettingsVariants::discussion::company_discussion_type::C"
msgid "Communication"
msgstr "Komunikacijos"

msgctxt "SettingsVariants::discussion::company_discussion_type::R"
msgid "Rating"
msgstr "Reitingas"

msgctxt "SettingsVariants::discussion::company_discussion_type::D"
msgid "Disabled"
msgstr "Neįgalinta"

msgctxt "SettingsVariants::discussion::product_post_approval::any"
msgid "Any customer"
msgstr "Visiems pirkėjams"

msgctxt "SettingsVariants::discussion::product_post_approval::anonymous"
msgid "Only anonymous customers"
msgstr "Tik anoniminiai pirkėjai"

msgctxt "SettingsVariants::discussion::product_post_approval::disabled"
msgid "No approval needed"
msgstr "Nereikia patvirtinimo"

msgctxt "SettingsVariants::discussion::category_post_approval::any"
msgid "Any customer"
msgstr "Visiems pirkėjams"

msgctxt "SettingsVariants::discussion::category_post_approval::anonymous"
msgid "Only anonymous customers"
msgstr "Tik anoniminiai pirkėjai"

msgctxt "SettingsVariants::discussion::category_post_approval::disabled"
msgid "No approval needed"
msgstr "Nereikia patvirtinimo"

msgctxt "SettingsVariants::discussion::page_post_approval::any"
msgid "Any customer"
msgstr "Visiems pirkėjams"

msgctxt "SettingsVariants::discussion::page_post_approval::anonymous"
msgid "Only anonymous customers"
msgstr "Tik anoniminiai pirkėjai"

msgctxt "SettingsVariants::discussion::page_post_approval::disabled"
msgid "No approval needed"
msgstr "Nereikia patvirtinimo"

msgctxt "SettingsVariants::discussion::home_page_post_approval::any"
msgid "Any customer"
msgstr "Visiems pirkėjams"

msgctxt "SettingsVariants::discussion::home_page_post_approval::anonymous"
msgid "Only anonymous customers"
msgstr "Tik anoniminiai pirkėjai"

msgctxt "SettingsVariants::discussion::home_page_post_approval::disabled"
msgid "No approval needed"
msgstr "Nereikia patvirtinimo"

msgctxt "SettingsVariants::discussion::home_page_testimonials::B"
msgid "Communication and Rating"
msgstr "Komunikacijos ir vertinimas"

msgctxt "SettingsVariants::discussion::home_page_testimonials::C"
msgid "Communication"
msgstr "Komunikacijos"

msgctxt "SettingsVariants::discussion::home_page_testimonials::R"
msgid "Rating"
msgstr "Reitingas"

msgctxt "SettingsVariants::discussion::home_page_testimonials::D"
msgid "Disabled"
msgstr "Neįgalinta"

msgctxt "Languages::discussion_tab_products"
msgid "Product Reviews"
msgstr ""

msgctxt "Languages::discussion_tab_categories"
msgid "Category Reviews"
msgstr ""

msgctxt "Languages::discussion_tab_pages"
msgid "Page Comments"
msgstr ""

msgctxt "Languages::discussion_tab_orders"
msgid "Order Communication"
msgstr ""

msgctxt "Languages::discussion_tab_home_page"
msgid "Testimonials"
msgstr ""

msgctxt "Languages::discussion_tab_companies"
msgid "Company Reviews"
msgstr ""

